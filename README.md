## POC / KISS farm-data-train container / notebook
* Collect data from different stations
* Run queries in separated environments without granting access to all the data

## How does the farm data train work? 
* A model / notebook is collecting data from each station
* Each station:
    * pulls the model from the Git repository
    * builds the container
    * run the notebook
    * add code
    * collect the data 
    * remove private keys / data
    * commit & push the project to Git

## Run the in the container
```bash
$ git pull git@git.wur.nl:kland001/farm-data-train.git
$ docker build -t fdt .
$ docker run -p 8888:8888 fdt -v .:/home
```
## Commit & push the project to the git repository
```bash
$ git commit . -m"added data from station2"
$ git push
```

## Links
* Preview the Notebook on Google Colaboratory https://colab.research.google.com/drive/1_XwFW8BEKRIyKtVr96iqqmUc407tZob1?usp=sharing
