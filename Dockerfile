# FROM ubuntu:18.04
from python:3.7.9-buster
RUN pip3 install jupyter
RUN pip3 install pandas
WORKDIR /home
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
